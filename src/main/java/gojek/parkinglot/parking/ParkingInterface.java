package gojek.parkinglot.parking;

import gojek.parkinglot.cars.Cars;

public interface ParkingInterface {
	String createSlot(int size);

	int getSizeSlot();

	void setSlot(Cars car);

	int getFreeSlot();

	String remove(int noslot);

	String getSlotWithRegNumAndColor();
}
