package gojek.parkinglot.cars;


public class Cars implements CarsInterface {
	String color;
	String regNumber;
	int slot;

	public Cars(String color, String regNumber, int slot) {
		this.color = color;
		this.regNumber = regNumber;
		this.slot = slot;
	}

	public String getColor() {
		return color;
	}

	public int getSlot() {
		return slot;
	}

	public String getRegNumber() {
		return regNumber;
	}

}