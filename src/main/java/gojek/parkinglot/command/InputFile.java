package gojek.parkinglot.command;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import gojek.parkinglot.parking.ParkingLot;

public class InputFile {

	public void inputFile(InputTerminal cmd,ParkingLot park) throws IOException {
		BufferedReader readfile = new BufferedReader(new FileReader(
				"./file_input.txt"));
		String line = readfile.readLine();
		while (line != null) {
			if (line.trim().length() == 0) {
				line = readfile.readLine();
				continue;
			}
			InputStream in = new ByteArrayInputStream(line.getBytes());
			System.setIn(in);
			cmd.commandInput(park);
			line = readfile.readLine();
		}
		readfile.close();
	}

}
