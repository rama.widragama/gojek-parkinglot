package gojek.parkinglot.cars;

public interface CarsInterface {
	String getColor();

	int getSlot();

	String getRegNumber();
}
