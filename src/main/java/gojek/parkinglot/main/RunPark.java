package gojek.parkinglot.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import gojek.parkinglot.command.InputFile;
import gojek.parkinglot.command.InputTerminal;
import gojek.parkinglot.parking.ParkingLot;


public class RunPark {

	public static void main(String[] args) throws IOException {
		System.out.println("Selamat datang di program Parking Lot");
		System.out.println("1.Untuk Input Menggunakan files");
		System.out.println("2.Input untuk menggunakan CMD");
		System.out.print("type '1' or '2' : ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ParkingLot park = new ParkingLot();
		InputTerminal cmd = new InputTerminal();
		String pilih = br.readLine();
		InputFile inf=new InputFile();
		System.out.println(pilih);
		if (pilih.equals("1")) {
			inf.inputFile(cmd, park);
		}

		else if (pilih.equals("2")) {
			String input = cmd.commandInput(park);
			while (!input .equals("exit")) input = cmd.commandInput(park);
			System.out.println("Terimakasih telah menggunakan Parking Lot");
		}
	}
}

