package gojek.parkinglot.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import gojek.parkinglot.cars.Cars;
import gojek.parkinglot.parking.ParkingLot;

public class InputTerminal {
	public String commandInput(ParkingLot park) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		String[] split = input.split(" ");
		switch (split[0]) {
		case ("create_parking_lot"):
			System.out.println(park.createSlot(Integer.parseInt(split[1])));

			break;
		case ("park"):
			if (park.getFreeSlot() == 0) {
				System.out.println("Sorry, parking lot is full");
			} else {
				int slot = park.getFreeSlot();
				park.setSlot(new Cars(split[2], split[1].toString(), slot - 1));
				System.out.println("Allocated slot number:" + slot);
			}
			break;
		case ("leave"):
			System.out.println(park.remove(Integer.parseInt(split[1])));
			break;
		case ("status"):
			System.out.println(park.getSlotWithRegNumAndColor());
			break;
		case ("registration_numbers_for_cars_with_colour"):
			System.out.println(park.getRegNumWithColor(split[1]));
			break;

		case ("slot_numbers_for_cars_with_colour"):
			System.out.println(park.getCarsWithColor(split[1]));
			break;
		case ("slot_number_for_registration_number"):
			System.out.println(park.getCarsWithRegNum(split[1]));
			break;
		}
		return input;
	}
}
