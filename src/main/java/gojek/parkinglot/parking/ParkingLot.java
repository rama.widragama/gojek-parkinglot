package gojek.parkinglot.parking;

import gojek.parkinglot.cars.Cars;
import gojek.parkinglot.parking.ParkingInterface;

public class ParkingLot implements ParkingInterface {
	int size;
	Cars[] slots;
	int initial;

	public ParkingLot() {
		this.size = 0;
		this.initial = 0;
	}

	public int getSizeSlot() {
		return slots.length;
	}

	public String createSlot(int size) {
		this.slots = new Cars[size];
		return "Created a parking lot with " + size + " slots";
	}

	public void setSlot(Cars cars) {
		int noslot = getFreeSlot();
		slots[noslot - 1] = cars;

	}

	public int getFreeSlot() {
		int freeslot = 0;
		for (int i = 0; i < slots.length; i++) {
			if (slots[i] == null) {
				freeslot = i + 1;
				break;
			}

		}
		return freeslot;
	}
	// find slot number using color or regnumber

	public String remove(int noslot) {
		slots[noslot - 1] = null;
		return "Slot number " + noslot + " is free";
	}

	public String getSlotWithRegNumAndColor() {
		String result = "Slot No. Registration No Colour ";
		for (Cars car : slots) {
			if (car == null) {
				continue;
			}
			int no = car.getSlot() + 1;
			String regNum = car.getRegNumber();
			String color = car.getColor();
			result = result + no + " " + regNum + " " + color + " ";

		}
		if (result == "Slot No. Registration No Colour ") {
			return "Not found";
		} else {
			return result.substring(0, result.length() - 1);
		}
	}

	public String getRegNumWithColor(String color) {
		String getNumRegWithColor = "";
		for (Cars car : slots) {
			if (car == null) {
				continue;
			}
			if (car.getColor().equals(color)) {
				String numReg = car.getRegNumber();
				getNumRegWithColor = getNumRegWithColor + numReg + ", ";
			}

		}
		if (getNumRegWithColor == "") {
			return "Not found";
		} else {
			return getNumRegWithColor.substring(0, getNumRegWithColor.length() - 2);
		}
	}

	public String getCarsWithColor(String color) {
		String getSlotWithColor = "";
		for (Cars car : slots) {
			if (car == null) {
				continue;
			}
			if (car.getColor().equals(color)) {
				int no = car.getSlot() + 1;
				getSlotWithColor = getSlotWithColor + no + ", ";
			}

		}

		if (getSlotWithColor == "") {
			return "Not found";
		}

		else {
			return getSlotWithColor.substring(0, getSlotWithColor.length() - 2);
		}
	}

	public String getCarsWithRegNum(String regNum) {
		String getSlotWithRegNum = "";
		for (Cars car : slots) {
			if (car == null) {
				continue;
			}

			if (car.getRegNumber().equalsIgnoreCase(regNum)) {
				int no = car.getSlot() + 1;
				getSlotWithRegNum = getSlotWithRegNum + no + ", ";
			}
		}
		if (getSlotWithRegNum == "") {
			return "Not found";
		} else {
			return getSlotWithRegNum.substring(0, getSlotWithRegNum.length() - 2);
		}
	}
}
