package gojek.parkinglot.parking;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.jupiter.api.Test;

import gojek.parkinglot.cars.Cars;
import gojek.parkinglot.command.InputTerminal;
import gojek.parkinglot.main.RunPark;
import gojek.parkinglot.parking.ParkingLot;

class ParkingServicesTest {

	static String TEST_REGN_NUMBER_1 = "KA-01-HH-1234";
	static String TEST_REGN_NUMBER_2 = "KA-01-HH-9999";
	static String TEST_REGN_NUMBER_3 = "KA-01-BB-0001";
	static String TEST_REGN_NUMBER_4 = "KA-01-HH-2701";

	static String COLOR_1 = "White";
	static String COLOR_2 = "White";
	static String COLOR_3 = "Black";
	static String COLOR_4 = "Blue";

	ParkingLot park = new ParkingLot();
	InputTerminal cmd = new InputTerminal();
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	@Test
	public void createParkingLotTest() {

		park.createSlot(6);
		assertEquals(6, park.getSizeSlot());
	}

	@Test
	public void testGetFreeSlotTest() {
		park.createSlot(1);
		int free = park.getFreeSlot();
		assertEquals(1, free);
	}

	@Test
	public void testSlotTest() {
		park.createSlot(1);
		Cars car = new Cars(COLOR_1, TEST_REGN_NUMBER_1, park.getFreeSlot() - 1);
		park.setSlot(car);
		assertEquals(0, park.getFreeSlot());
	}

	@Test
	public void leaveSlotTest() {
		park.createSlot(1);
		Cars car = new Cars(COLOR_1, TEST_REGN_NUMBER_1, park.getFreeSlot() - 1);
		park.setSlot(car);
		park.remove(1);
		assertEquals(1, park.getFreeSlot());
	}

	@Test
	public void getSlotWithRegAndNumTest() {
		park.createSlot(4);
		Cars car = new Cars(COLOR_1, TEST_REGN_NUMBER_1, park.getFreeSlot() - 1);
		park.setSlot(car);
		Cars car2 = new Cars(COLOR_2, TEST_REGN_NUMBER_2, park.getFreeSlot() - 1);
		park.setSlot(car2);
		Cars car3 = new Cars(COLOR_3, TEST_REGN_NUMBER_3, park.getFreeSlot() - 1);
		park.setSlot(car3);
		Cars car4 = new Cars(COLOR_4, TEST_REGN_NUMBER_4, park.getFreeSlot() - 1);
		park.setSlot(car4);
		String result = park.getSlotWithRegNumAndColor();
		assertEquals(
				"Slot No. Registration No Colour 1 KA-01-HH-1234 White 2 KA-01-HH-9999 White 3 KA-01-BB-0001 Black 4 KA-01-HH-2701 Blue",
				result);
	}

	@Test
	public void getRegNumWithColorTestFound() {
		park.createSlot(1);
		Cars car = new Cars(COLOR_1, TEST_REGN_NUMBER_1, park.getFreeSlot() - 1);
		park.setSlot(car);
		String result = park.getRegNumWithColor(car.getColor());
		assertEquals(car.getRegNumber(), result);
	}

	@Test
	public void getRegNumWithColorTestNotFound() {
		park.createSlot(1);
		Cars car = new Cars(COLOR_1, TEST_REGN_NUMBER_1, park.getFreeSlot() - 1);
		park.setSlot(car);
		String result = park.getRegNumWithColor("Yellow");
		assertEquals("Not found", result);
	}

	@Test
	public void getCarsWithColorTestFound() {
		park.createSlot(1);
		Cars car = new Cars(COLOR_1, TEST_REGN_NUMBER_1, park.getFreeSlot() - 1);
		park.setSlot(car);
		String result = park.getCarsWithColor(car.getColor());
		assertEquals("1", result);

	}

	@Test
	public void getCarsWithColorTestNotFound() {
		park.createSlot(1);
		Cars car = new Cars(COLOR_1, TEST_REGN_NUMBER_1, park.getFreeSlot() - 1);
		park.setSlot(car);
		String result = park.getCarsWithColor("Yellow");
		assertEquals("Not found", result);

	}

	@Test
	public void getCarsWithRegNumFound() {
		park.createSlot(1);
		Cars car = new Cars(COLOR_1, TEST_REGN_NUMBER_1, park.getFreeSlot() - 1);
		park.setSlot(car);
		String result = park.getCarsWithRegNum(car.getRegNumber());
		assertEquals("1", result);
	}

	@Test
	public void getCarsWithRegNumNotFound() {
		park.createSlot(1);
		Cars car = new Cars(COLOR_1, TEST_REGN_NUMBER_1, park.getFreeSlot() - 1);
		park.setSlot(car);
		String result = park.getCarsWithRegNum(TEST_REGN_NUMBER_2);
		assertEquals("Not found", result);
	}

	@Test
	public void createParkingLotInput() throws IOException {
		InputTerminal cmd = new InputTerminal();
		ParkingLot park = new ParkingLot();
		String pilih = "1";
		InputStream in = new ByteArrayInputStream(pilih.getBytes());
		System.setIn(in);
		RunPark.main(null);

		String input = "create_parking_lot 1";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("create_parking_lot 1", cmd.commandInput(park));

		input = "park DL-12-AA-9999 White";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("park DL-12-AA-9999 White", cmd.commandInput(park));

		input = "park DL-12-AA-9998 White";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("park DL-12-AA-9998 White", cmd.commandInput(park));

		input = "status Slot No. Registration No Colour";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("status Slot No. Registration No Colour", cmd.commandInput(park));

		input = "leave 1";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("leave 1", cmd.commandInput(park));

		input = "park DL-12-AA-9999 White";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		input = "registration_numbers_for_cars_with_colour White";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("registration_numbers_for_cars_with_colour White", cmd.commandInput(park));

		input = "slot_numbers_for_cars_with_colour White";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("slot_numbers_for_cars_with_colour White", cmd.commandInput(park));

		input = "status Slot No. Registration No Colour";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("status Slot No. Registration No Colour", cmd.commandInput(park));

		input = "slot_number_for_registration_number DL-12-AA-9999";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("slot_number_for_registration_number DL-12-AA-9999", cmd.commandInput(park));

		input = "slot_number_for_registration_number DL-12-AA-10000";
		in = new ByteArrayInputStream(input.getBytes());
		System.setIn(in);

		assertEquals("slot_number_for_registration_number DL-12-AA-10000", cmd.commandInput(park));

	
	}

}
